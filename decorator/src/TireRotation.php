<?php
/**
 * Author: Cory Norris
 * Created At: 15/06/15, 11:10 PM
 */

namespace Decorator;

class TireRotation implements CarService
{

    protected $carService;

    function __construct($carService)
    {
        $this->carService = $carService;
    }

    public function getCost()
    {
        return 15 + $this->carService->getCost();
    }

    public function getDescription()
    {
        return $this->carService->getDescription() . ', and a Tire Rotation';
    }
}