<?php
/**
 * Author: Cory Norris
 * Created At: 15/06/15, 11:05 PM
 */

namespace Decorator;

class OilChange implements CarService
{

    protected $carService;

    function __construct($carService)
    {
        $this->carService = $carService;
    }


    public function getCost()
    {
        return 29 + $this->carService->getCost();
    }

    public function getDescription()
    {
        return $this->carService->getDescription() . ', and an Oil Change';
    }
}