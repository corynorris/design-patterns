<?php
/**
 * Author: Cory Norris
 * Created At:
 */

namespace Decorator;

class BasicInspection implements CarService
{

    /**
     * @return int
     */
    public function getCost()
    {
        return 25;
    }


    public function getDescription()
    {
        return 'Basic Inspection';
    }
}
