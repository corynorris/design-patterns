<?php
/**
 * Author: Cory Norris
 * Created At: 15/06/15, 11:04 PM
 */

namespace Decorator;

interface CarService
{

    public function getCost();

    public function getDescription();
}