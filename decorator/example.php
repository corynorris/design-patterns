<?php
/**
 * Author: Cory Norris
 * Created At: 15/06/15, 11:15 PM
 */

use Decorator\BasicInspection;
use Decorator\OilChange;
use Decorator\TireRotation;


echo 'done imports';

$service = new OilChange(new TireRotation(new BasicInspection));

echo '<pre>' . var_export($service, true) . '</pre>';

echo $service->getCost() . '\n';

echo $service->getDescription() . '.';