<?php
/**
 * Author: Cory Norris
 * Created At: 16/06/15, 10:39 PM
 */


namespace ChainOfResponsibility;

use Exception;


class Lights extends HomeChecker
{

    public function check(HomeStatus $home)
    {
        if ( ! $home->lightsOff)
        {
            throw new Exception('The lights have not been turned off!! Abort.');
        }

        $this->next($home);
    }
}