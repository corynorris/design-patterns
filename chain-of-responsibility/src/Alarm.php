<?php
/**
 * Author: Cory Norris
 * Created At: 16/06/15, 10:39 PM
 */


namespace ChainOfResponsibility;

use Exception;

class Alarm extends HomeChecker
{

    public function check(HomeStatus $home)
    {
        if ( ! $home->alarmOn)
        {
            throw new Exception('The alarm has not been set!! Abort.');
        }

        $this->next($home);
    }
}