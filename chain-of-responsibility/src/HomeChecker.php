<?php
/**
 * Author: Cory Norris
 * Created At: 16/06/15, 10:42 PM
 */


namespace ChainOfResponsibility;


abstract class HomeChecker
{

    protected $successor;

    public abstract function check(HomeStatus $home);

    public function succeedWith(HomeChecker $successor)
    {
        $this->successor = $successor;
    }

    public function next(HomeStatus $home)
    {
        if ($this->successor)
        {
            $this->successor->check($home);
        }
    }


}