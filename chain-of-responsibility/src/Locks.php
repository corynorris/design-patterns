<?php
/**
 * Author: Cory Norris
 * Created At: 16/06/15, 10:39 PM
 */


namespace ChainOfResponsibility;


use Exception;

class Locks extends HomeChecker
{

    public function check(HomeStatus $home)
    {
        if ( ! $home->locked)
        {
            throw new Exception('The doors are not locked!! Abort.');
        }

        $this->next($home);
    }
}