<?php
/**
 * Author: Cory Norris
 * Created At: 16/06/15, 10:40 PM
 */


namespace ChainOfResponsibility;


class HomeStatus
{

    public $alarmOn = true;
    public $locked = true;
    public $lightsOff = true;
}