<?php
/**
 * Author: Cory Norris
 * Created At: 16/06/15, 11:16 AM
 */


use ChainOfResponsibility\Alarm;
use ChainOfResponsibility\HomeStatus;
use ChainOfResponsibility\Lights;
use ChainOfResponsibility\Locks;

$locks = new Locks();
$lights = new Lights();
$alarm = new Alarm();

$locks->succeedWith($lights);
$lights->succeedWith($alarm);

try
{
    $homeStatus = new HomeStatus;
    $homeStatus->lightsOff = false; //should throw lights off exception
    $locks->check($homeStatus);
} catch (Exception $e)
{
    echo $e->getMessage() . "/n";
}
