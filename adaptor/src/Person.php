<?php
/**
 * Author: Cory Norris
 * Created At: 16/06/15, 10:16 AM
 */

namespace Adaptor;

class Person
{

    public function read(BookInterface $book)
    {
        $book->open();
        $book->turnPage();
    }
}