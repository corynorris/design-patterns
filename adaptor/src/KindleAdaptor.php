<?php
/**
 * Author: Cory Norris
 * Created At: 16/06/15, 10:23 AM
 */


namespace Adaptor;


class KindleAdaptor implements BookInterface
{

    private $kindle;

    function __construct($kindle)
    {
        $this->kindle = $kindle;
    }


    public function open()
    {
        return $this->kindle->turnOn();
    }

    public function turnPage()
    {
        return $this->kindle->pressNextButton();
    }
}