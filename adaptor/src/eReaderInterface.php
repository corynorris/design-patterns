<?php
/**
 * Author: Cory Norris
 * Created At: 16/06/15, 10:23 AM
 */
namespace Adaptor;

interface eReaderInterface
{

    public function turnOn();

    public function pressNextButton();
}