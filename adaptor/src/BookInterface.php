<?php
/**
 * Author: Cory Norris
 * Created At: 16/06/15, 10:21 AM
 */
namespace Adaptor;

interface BookInterface
{

    public function open();

    public function turnPage();
}