<?php
/**
 * Author: Cory Norris
 * Created At: 16/06/15, 10:17 AM
 */

namespace Adaptor;

class Book implements BookInterface
{

    public function open()
    {
        var_dump('opening a paper book');
    }

    public function turnPage()
    {
        var_dump('turning the page of the paper book');
    }

}