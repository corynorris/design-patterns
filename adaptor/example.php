<?php
/**
 * Author: Cory Norris
 * Created At: 15/06/15, 11:15 PM
 */

use Adaptor\Book;
use Adaptor\Kindle;
use Adaptor\KindleAdaptor;
use Adaptor\Person;

$person = new Person;
$person->read(new Book);

$person->read(new KindleAdaptor(new Kindle()));

// An example in Laravel is the FilesystemAdaptor Facade.