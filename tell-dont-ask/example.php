<?php
/**
 * Author: Cory Norris
 * Created At: 16/06/15, 11:07 PM
 */

use TellDontAsk\Elevator;
use TellDontAsk\EnvironmentMonitor;

$elevator = new Elevator(new EnvironmentMonitor());

$elevator->addPerson('Peter Griffen');
$elevator->addPerson('Stan Smith');
$elevator->addPerson('John Deer');

try
{
    $elevator->checkForAlarms();
} catch (Exception $e)
{
    echo $e->getMessage() . "\n";
}