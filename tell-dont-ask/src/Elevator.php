<?php
/**
 * Author: Cory Norris
 * Created At: 16/06/15, 11:05 PM
 */


namespace TellDontAsk;


class Elevator
{

    protected $monitor;

    protected $occupants = [];

    function __construct(EnvironmentMonitor $monitor)
    {
        $this->monitor = $monitor;
    }

    public function addPerson($person)
    {
        $this->occupants[] = $person;

        $this->monitor->increaseTemperature();
    }

    public function checkForAlarms()
    {
        $this->monitor->checkForAlarms();
    }

}