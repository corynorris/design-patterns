<?php
/**
 * Author: Cory Norris
 * Created At: 16/06/15, 11:06 PM
 */


namespace TellDontAsk;


use Exception;

class EnvironmentMonitor
{

    protected $temperature = 70;

    public function increaseTemperature($degrees = 10)
    {
        $this->temperature += $degrees;
    }

    public function checkForAlarms()
    {
        if ($this->temperature >= 100)
        {
            $this->triggerAlarm();
        }
    }

    private function triggerAlarm()
    {
        throw new Exception('Too hot! Alarm triggered!');
    }

}