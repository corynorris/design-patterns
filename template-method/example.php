<?php
/**
 * Author: Cory Norris
 * Created At: 15/06/15, 11:15 PM
 */


$turkeySub = new TemplateMethod\TurkeySub();
$veggieSub = new TemplateMethod\VeggieSub();

$turkeySub->make();

echo '<br />';

$veggieSub->make();