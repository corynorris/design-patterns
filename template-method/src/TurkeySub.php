<?php
/**
 * Author: Cory Norris
 * Created At: 16/06/15, 10:37 AM
 */

namespace TemplateMethod;

class TurkeySub extends Sub
{

    protected function addPrimaryToppings()
    {
        var_dump('Adding Turkey');

        return $this;
    }

}