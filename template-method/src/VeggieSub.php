<?php
/**
 * Author: Cory Norris
 * Created At: 16/06/15, 10:40 AM
 */


namespace TemplateMethod;


class VeggieSub extends Sub
{

    protected function addPrimaryToppings()
    {
        var_dump('Adding Veggies');

        return $this;
    }
}