<?php
/**
 * Author: Cory Norris
 * Created At: 16/06/15, 10:46 AM
 */


namespace TemplateMethod;


abstract class Sub
{


    public function make()
    {
        return $this
            ->layBread()
            ->addLettuce()
            ->addSauces()
            ->addPrimaryToppings();
    }

    protected abstract function addPrimaryToppings();

    protected function addSauces()
    {
        var_dump('Adding Sauces');

        return $this;
    }

    protected function addLettuce()
    {
        var_dump('Adding Lettuce');

        return $this;
    }

    protected function layBread()
    {
        var_dump('Laying Bread');

        return $this;
    }
}