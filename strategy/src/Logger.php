<?php
/**
 * Author: Cory Norris
 * Created At: 16/06/15, 11:05 AM
 */


namespace Strategy;


interface Logger
{

    public function log($data);

}