<?php
/**
 * Author: Cory Norris
 * Created At: 16/06/15, 11:05 AM
 */


namespace Strategy;


class LogToDatabase implements Logger
{

    public function log($data)
    {
        var_dump('Log the data to a database: ' . $data);
    }
}