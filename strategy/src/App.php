<?php
/**
 * Author: Cory Norris
 * Created At: 16/06/15, 11:08 AM
 */


namespace Strategy;


class App
{

    public function log($data, Logger $logger = null)
    {
        $logger = $logger ?: new LogToFile();
        $logger->log($data);
    }
}