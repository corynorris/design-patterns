<?php
/**
 * Author: Cory Norris
 * Created At: 16/06/15, 11:03 AM
 */


use Strategy\App;
use Strategy\LogToDatabase;

$app = new App;

$app->log('data goes here', New LogToDatabase());

$app->log('more data, no logger specified');