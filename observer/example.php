<?php
/**
 * Author: Cory Norris
 * Created At: 15/06/15, 11:15 PM
 */

use Observer\EmailNotifier;
use Observer\LogHandler;
use Observer\Login;

$login = new Login;


$login->attach([
    new LogHandler(),
    new EmailNotifier()
]);

$login->fire();