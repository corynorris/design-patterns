<?php
/**
 * Author: Cory Norris
 * Created At: 16/06/15, 12:30 AM
 */

namespace Observer;

// aka subscriber
interface Observer
{

    public function handle();
}