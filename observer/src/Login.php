<?php
/**
 * Author: Cory Norris
 * Created At: 16/06/15, 12:34 AM
 */

namespace Observer;

class Login implements Subject
{

    protected $observers = [];


    public function attach($observable)
    {
        if (is_array($observable))
        {
            return $this->attachObservers($observable);
        }

        $this->observers[] = $observable;
    }

    /**
     * @param $observable
     * @throws \Exception
     */
    public function attachObservers($observable)
    {
        foreach ($observable as $observer)
        {

            if ( ! $observer instanceof Observer)
            {
                throw new \Exception;
            }

            $this->attach($observer);

        }
    }

    public function detach($index)
    {
        unset($this->observers[$index]);
    }

    public function fire()
    {
        $this->notify();
    }

    public function notify()
    {
        foreach ($this->observers as $observer)
        {
            $observer->handle();
        }
    }
}