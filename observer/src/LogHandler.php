<?php
/**
 * Author: Cory Norris
 * Created At: 16/06/15, 12:40 AM
 */


namespace Observer;


class LogHandler implements Observer
{

    public function handle()
    {
        var_dump('log something important');
    }
}